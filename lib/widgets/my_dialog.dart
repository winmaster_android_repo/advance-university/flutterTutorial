import 'package:flutter/material.dart';

class MyDialog extends StatefulWidget
{
  final bool _isLiked;
  final VoidCallback onTap;

  MyDialog(this._isLiked, this.onTap);

  @override
  State createState() => new DialogState();
}
class DialogState extends State<MyDialog>
{
  @override
  Widget build(BuildContext context)
  {
    return new Material(
      color: Colors.black45,
      child: new InkWell(
        onTap: () => widget.onTap(),
        child:
        new Container(
          width: MediaQuery.of(context).size.width, //wielkość równa szerokości ekranu
          decoration: new BoxDecoration( color: Colors.black54, border: new Border.all(color: Colors.white, width: 5.0)),
          child:  new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              child: new Icon(widget._isLiked == true ? Icons.thumb_up : Icons.thumb_down, size: 120.0, color: Colors.blueGrey,),
            ),
            new Padding( padding: new EdgeInsets.only(bottom: 30.0),
            ),
            new Text(widget._isLiked == true ? "You liked it!" : "I see you don't like it",
              style: new TextStyle(color: Colors.white, fontSize: 40.0), textAlign: TextAlign.center,)
          ],
        ),
      ),
    ),
    );
  }
}