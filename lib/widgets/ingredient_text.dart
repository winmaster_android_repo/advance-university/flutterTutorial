import 'package:flutter/material.dart';

class IngredientText extends StatefulWidget
{
  final String _ingredient;

  IngredientText(this._ingredient);

  @override
  State createState() => new IngredientTextState();
}
  class  IngredientTextState extends State<IngredientText> with SingleTickerProviderStateMixin
  {
    Animation<double> _fontSizeAnimation;
    AnimationController _fontSizeAnimationController;

    @override
    void initState()
    {
      super.initState();
      _fontSizeAnimationController = new AnimationController(duration: new Duration(milliseconds: 500), vsync: this);
      _fontSizeAnimation = new CurvedAnimation(parent: _fontSizeAnimationController, curve: Curves.bounceIn);
      _fontSizeAnimation.addListener(() => this.setState(() => {}));
      _fontSizeAnimationController.forward();
    }

    @override
    void dispose() {
      _fontSizeAnimationController.dispose();
      super.dispose();
    }

    @override
    Widget build(BuildContext context)
    {
          return new Material(
            color: Colors.white,
            child: new Padding(
                padding: new EdgeInsets.symmetric(vertical: 20.0),
                child: new Center(
                  child: new Text(widget._ingredient, style: new TextStyle(fontSize: _fontSizeAnimation.value * 25)),
                )
            ),
      );
    }
  }