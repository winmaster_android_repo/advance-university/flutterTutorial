class Ingredient
{
   final String ingredientName;
   final bool state;

  Ingredient(this.ingredientName, [this.state = false]);
}