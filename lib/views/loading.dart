import 'package:flutter/material.dart';
import '../views/ingredient_view.dart';

class LoadingPage extends StatefulWidget {

  @override
  State createState() => new LoadingPageState();
}

class LoadingPageState extends State<LoadingPage> with SingleTickerProviderStateMixin
{
  Animation<double> _photoAnimation;
  AnimationController _photoAnimationController;

  @override
  void initState() {
    super.initState();
    _photoAnimationController = new AnimationController(duration: new Duration(seconds: 5), vsync: this);
    _photoAnimation = new CurvedAnimation(parent: _photoAnimationController, curve: Curves.elasticOut);
    _photoAnimation.addListener(() => this.setState(() {}));
    _photoAnimationController.forward();
  }

  @override
  void dispose() {
    _photoAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context)
  {
    return new Material(
      color: Colors.blueAccent,
      child: new InkWell(
        onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new IngredientView())),
        child:
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:<Widget>[
              new Stack(children: <Widget>[new Transform.rotate(
              angle: _photoAnimation.value * 5,
                child: new Image.asset("images/pizza.png", width:  _photoAnimation.value * 200.0 , height: _photoAnimation.value * 200.0),
              ),]),
              new Text("Pizza maker!", style: new TextStyle(color:Colors.white, fontSize: 45.0, fontWeight: FontWeight.bold),),
              new Text("Tap to start", style: new TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),)
            ],
          ),
        ),
    );
  }
}

