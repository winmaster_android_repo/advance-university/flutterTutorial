import 'package:flutter/material.dart';
import '../views/loading.dart';

class EndPage extends StatelessWidget
{

  @override
  Widget build(BuildContext context)
  {
    return new Material(
        color: Colors.blueAccent,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text("Thank you!", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30.0)),
            new Text("Your pizza'll be awesome!", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20.0)),
            new IconButton(
              icon: new Icon(Icons.favorite),
              color: Colors.white,
              iconSize: 70.0,
                onPressed: () => Navigator.of(context).pushAndRemoveUntil(new MaterialPageRoute(builder: (BuildContext context) => new LoadingPage()), (Route route) => route == null)
            )
          ],
        )
    );
  }
}